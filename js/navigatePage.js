'use strict';
// Code for the Navigate page.

var pathIndex = localStorage.getItem(APP_PREFIX + "-selectedPath");
// If path from Main page is selected, retrieve its title and locations.
if (pathIndex !== null) {
    
    var pathTitle = availablePaths[pathIndex].title;
    var pathLocations = availablePaths[pathIndex].locations;
    
    var newPath = new Path(pathTitle, pathLocations);
    var pathName = newPath.getTitle();

    // If a path index was specified, show name in header bar title.    
    document.getElementById("headerBarTitle").textContent = pathName;
}

// Array of the selected path's locations.
var pathList = newPath.getLocations();

// Creates variables for 'initMap' function.
var map, lineSymbol;
var polyline, polylineHistory;
var marker, circle;

// Map Initialisation callback.  Will be called when Maps API loads.
function initMap() {
    // Initialise map, centred on current selected location.
    map = new google.maps.Map(document.getElementById('map'), {
        center: pathList[0],
        zoom: 17
    });
    
    // Creates infoWindow to show the start and the end of path
    var infoWindow = new google.maps.InfoWindow({
        position: pathList[0],
        content: "Start Here",
        disableAutoPan: true
    });
    infoWindow.open(map);

    var infoWindow = new google.maps.InfoWindow({
        position: pathList[(pathList.length)-1],
        content: "Ends Here",
        disableAutoPan: true
    });
    infoWindow.open(map);
    
    // Creates dashed polyline that points to next waypoint
    // Define a symbol using SVG path notation, with an opacity of 1.
    lineSymbol = {
      path: 'M 0,-1 0,1',
      strokeOpacity: 1,
      scale: 2
    };
    polyline = new google.maps.Polyline({
        map: map,
        geodesic: true,
        icons: [{
            icon: lineSymbol,
            offset: '0',
            repeat: '10px'
        }],
        strokeColor: '#FF0000',
        strokeOpacity: 0,
        strokeWeight: 1
    });
    
    // Creates marker that displays user's location and heading
    marker = new google.maps.Marker({
        map: map,
        animation: google.maps.Animation.DROP,
        title: "This is your current location"
    });
    
    // Creates circle around marker which represents accuracy
    circle = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: map
      });
}

// Variables for 'trackPosition' function:
// Keeping track of current waypoint
var currentWayPoint = 0;
// Stores previous GPS locations (lat and lng)
var walkedPath1, walkedPath2;
// Stores user heading
var heading;
// Get current time in seconds (to calculate speed)
var time1 = Date.now()/1000;

// Callback function for navigator.geolocation
function trackPosition(position) {
    // Get current location from GPS coordinates (updated when user's location changes)
    var location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    
    // Set map centered on current location
    map.setCenter(location);
    
    // Get current position accuracy
    var accuracy = position.coords.accuracy;
    
    // Get LatLng of the target waypoint from path
    var waypoint = new google.maps.LatLng(pathList[currentWayPoint].lat, pathList[currentWayPoint].lng);
    // Calculate distance between current location and target waypoint,
    // is also the remaining path distance
    var distance = google.maps.geometry.spherical.computeDistanceBetween(location, waypoint);
    
    // Once the distance between the user’s location and the waypoint of interest drops below the accuracy, 
    // assume they have reached the point and direct them to next waypoint
    if (distance <= accuracy) {
        if (currentWayPoint < (pathList.length)-1) {
            // Directing to next waypoint as long as they haven't reach the final coordinates of the location
            currentWayPoint++
        } 
        else if (currentWayPoint === (pathList.length)-1) {
            // Notifies user when they have completed the route (reached final coordinates)
            alert("You have reached the destination.")
            document.getElementById("nextAction").innerHTML = "Arrived";
        }
    }
    // Set dashed polyline path to point to the next waypoint (updated each time user's location changes)
    polyline.setPath([location, pathList[currentWayPoint]]);
    
    // Compute user's heading between current location and next waypoint
    heading = google.maps.geometry.spherical.computeHeading(location, waypoint);
    // Set map's heading according to user's heading
    map.setHeading(heading);
    
    // Set marker position to user's location
    marker.setPosition(location);
    // Set marker icon to arrow that reflects on user's location and relative direction (heading)
    marker.setIcon({
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
        scale: 3,
        rotation: position.coords.heading
    });
    
    // Set circle centered to current location, and also reflects the accuracy
    circle.setCenter(location);
    circle.setRadius(accuracy);
    
    // Stores previous GPS locations as user's history,
    // updated as user's location moves
    walkedPath1 = walkedPath2;
    walkedPath2 = location;
    
    // Generate a path by using Google polyline API using user location history,
    // line from starting position to current location
    polylineHistory = new google.maps.Polyline({
        map: map,
        path: [walkedPath1, walkedPath2],
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1,
        strokeWeight: 2
    });
    
    // When accuracy is accurate enough, it's represented by green circle. If not, the circle will be red.
    if (accuracy <= 20) {
        circle.setOptions({
            strokeColor: '#00FF00',
            fillColor: '#00FF00'
          });
    } else {
        circle.setOptions({
            strokeColor: '#FF0000',
            fillColor: '#FF0000'
          });
    }
    
    // Calculate total distance between current location and final location.
    var totalDistance = parseFloat(newPath.calculateDistance()) + distance;
    // Ratio between current remaining distance and total distance
    var ratio = distance/totalDistance;
    
    // As remaining distance reduces, the colour of the line to next point gets ligther
    if (ratio < 0.3) {
        // End colour is set to opacity 0.3
        lineSymbol.strokeOpacity = 0.3;
        polylineHistory.strokeOpacity = 0.3;
    }
    else {
        // Changing line opacity according to ratio
        lineSymbol.strokeOpacity = ratio;
        polylineHistory.strokeOpacity = ratio;
    }
    
    // Get time when function is called, is seconds
    var time2 = Date.now()/1000;
    // Calculate user speed, in m/s
    var speed = distance/(time2-time1);
    // Calculate ETA based on speed and time, in minutes
    var estimatedTime = (totalDistance/speed)/60;
    
    // Displays remaining path distance, speed, and ETA to screen
    document.getElementById("distance").innerHTML = distance.toFixed(1) + " m"
    document.getElementById("speed").innerHTML = speed.toFixed(1) + " m/s"
    document.getElementById("time").innerHTML = Math.round(estimatedTime) + " minutes"
}

// Function to handle location errors, e.g. when GPS is disabled
function showError() {
    alert("Location can't be found");
}

// Listener to respond to changes in GPS coordinates (geolocation API)
if (navigator.geolocation) {
    navigator.geolocation.watchPosition(trackPosition, showError, {enableHighAccuracy: true});
} else {
    alert("Your browser does not support Geolocation.");
}

// Using 'DeviceOrientation' to determine relative direction from current location to target waypoint
function handleOrientation(event) {
    var alpha = event.alpha;
    
    // Represents user's current position (degrees)
    marker.setIcon({
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
        scale: 3,
        rotation: alpha
    });
    
    // Determining relative direction, 
    // represented by the difference in the degree where user is heading and degree of location
    var direction = heading - alpha;
    
    // Conditions to determine what action should user take next,
    // according to calculated relative direction (or bearing)
    // and displays them graphically and textually on screen
    if (Math.abs(direction) >= 0 && Math.abs(direction) <= 20) {
        document.getElementById("nextAction").innerHTML = "Head Straight";
        document.getElementById("nextActionImg").src = "images/straight.svg";
    }
    
    else if (Math.abs(direction) >= 135 && Math.abs(direction) <= 220) {
        document.getElementById("nextAction").innerHTML = "Turn around";
        document.getElementById("nextActionImg").src = "images/uturn.svg";
    }
    
    else if (direction >= -65 && direction < -20 || direction >= 295 && direction <= 340) {
        document.getElementById("nextAction").innerHTML = "Slight Left";
        document.getElementById("nextActionImg").src = "images/slight_left.svg";
    }

    else if (direction >= -135 && direction < -65 || direction > 220 && direction < 295) {
        document.getElementById("nextAction").innerHTML = "Turn Left";
        document.getElementById("nextActionImg").src = "images/left.svg";
    }

    else if (Math.abs(direction) >= 65 && Math.abs(direction) <= 135 || direction >= -280 && direction < -220) {
        document.getElementById("nextAction").innerHTML = "Turn Right";
        document.getElementById("nextActionImg").src = "images/right.svg";
    }

    else if (direction > 20 && direction <= 65  || direction >= -340 && direction <= -295) {
        document.getElementById("nextAction").innerHTML = "Slight Right";
        document.getElementById("nextActionImg").src = "images/slight_right.svg";
    }
    
}

window.addEventListener("deviceorientation", handleOrientation, true);