'use strict';
// Shared code needed by all pages of the app.

// Prefix to use for Local Storage.
var APP_PREFIX = "monash.eng1003.navigationApp";

// Array of saved Path objects.
var availablePaths = [];


// This function serialises the array of paths in Clayton Campus to JSON,
// and stores this JSON string to 'ClaytonCampus' key in localStorage
function storeClayton(json) {
    var jsonRoutes = JSON.stringify(json);
    localStorage.setItem('ClaytonCampus',jsonRoutes);
}

// This function serialises the array of paths in Sunway Campus to JSON,
// and stores this JSON string to 'SunwayCampus' key in localStorage.
function storeSunway(json) {
    var jsonRoutes = JSON.stringify(json);
    localStorage.setItem('SunwayCampus',jsonRoutes);
}


// Contacts the Campus Nav web service, 
// campus is specified to 'clayton' in the query string.
// The returned JSON is passed to 'storeClayton' callback function.
var x = document.createElement('script');
x.src = 'https://eng1003.monash/api/campusnav/?campus=clayton&callback=storeClayton';
document.body.appendChild(x);

// Contacts the Campus Nav web service, 
// campus is specified to 'sunway' in the query string.
// The returned JSON is passed to 'storeSunway' callback function.
var y = document.createElement('script');
y.src = 'https://eng1003.monash/api/campusnav/?campus=sunway&callback=storeSunway';
document.body.appendChild(y);

// Get or load the paths from localStorage as JSON,
// the JSON is parsed into array of objects with path details of each campus.
var getClayton = localStorage.getItem('ClaytonCampus');
var clayton = JSON.parse(getClayton);
var getSunway = localStorage.getItem('SunwayCampus');
var sunway = JSON.parse(getSunway);

// Populate 'availablePaths' array with parsed path details from 'clayton'.
for (var i = 0; i < clayton.length; i++) {
    availablePaths.push(clayton[i]);
}
// Populate 'availablePaths' array with parsed path details from 'sunway'.
for (var i = 0; i < sunway.length; i++) {
    availablePaths.push(sunway[i]);
}


// Path class, with required attributes of:
// title (initTitle) and ordered array of location (initLocations).
function Path(initTitle, initLocations) {
    // Private attributes:

    var title;    
    var locations;     

    // Private methods:

    var setTitle = function(newTitle) {
            title = newTitle;
       };

    var setLocations = function(newLocations) {
            locations = newLocations;
    };

    // Initialisation
    
    setTitle(initTitle);
    setLocations(initLocations);

    // Public methods:
    
    this.getTitle = function() {
        return title;
    };

    this.getLocations = function() {
        return locations;
    };
    
    // Method that calculates distance of 2 locations
    this.getDistance = function(lat1, lng1, lat2, lng2) {
        // Formula retrieved from https://www.movable-type.co.uk/scripts/latlong.html
        var R = 6371000; // metres
        var phi1 = lat1 * Math.PI / 180;
        var phi2 = lat2 * Math.PI / 180;
        var deltaphi = (lat2-lat1)* Math.PI / 180;
        var deltalambda = (lng2-lng1)* Math.PI / 180;

        var a = Math.sin(deltaphi/2) * Math.sin(deltaphi/2) + Math.cos(phi1) * Math.cos(phi2) * Math.sin(deltalambda/2) * Math.sin(deltalambda/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        var d = R * c;
        
        return d;
    };
    
    // Method that calculates total distance of ordered array of locations
    this.calculateDistance = function() {
        var total = 0;
        for (var i = 0; i < (locations.length)-1; i++) {
            total += this.getDistance(locations[i].lat, locations[i].lng, locations[i+1].lat, locations[i+1].lng);
        }
        return total.toFixed(2);
    };
    
    // Method that calculates the number of turns or points of the path
    this.getTurns = function() {
        var turns = locations.length;
        return turns;
    };

}    