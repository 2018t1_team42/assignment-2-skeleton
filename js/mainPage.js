'use strict';
// Code for the Main page of the app.

// Takes user to Navigation page for the selected Path (by clicking on one of the list entries).
function viewPath(pathIndex) {
    // Save the selected path index to local storage so it can be accessed
    // from the Navigate page.
    localStorage.setItem(APP_PREFIX + "-selectedPath", pathIndex);
    // ... and then load the Navigate page.
    document.location.href = 'navigate.html';
}

// Iterates through list of paths ('availablePaths' array) that match the paths in localStorage.
for (var i = 0; i < availablePaths.length; i++) {
    // Creates new Path class instances
    window['path'+i] = new Path(availablePaths[i].title, availablePaths[i].locations);
    
    // Displays total distance and number of turns of the Path with method from Path class
    document.getElementById('path'+i).innerHTML = window['path'+i].calculateDistance() + ' metres, ' + window['path'+i].getTurns() + ' turns.';
    
    // Displays title of the path
    document.getElementById('title'+i).innerHTML = window['path'+i].getTitle();
}